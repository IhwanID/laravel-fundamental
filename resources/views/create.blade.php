@extends('layouts.global')
@section('title')
Create Blog Post
@endsection
@section('content')
@include('partials.menu')
<h1>Halaman Create Post</h1>
<form action="{{url('/blog')}}" method="POST">
    @csrf
    <label for="title">Judul</label>
    <input type="text" name="title" id="title">
    <br>
    <label for="body">isi</label>
    <textarea name="body" id="body" cols="30" rows="10"></textarea>
    <br>
    <button type="submit">Kirim</button>
</form>
@endsection
