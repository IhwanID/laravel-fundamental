@extends('layouts.global')

@section('title')
Halaman Index
@endsection

@section('content')

@include('partials.menu')
<h1>Halaman Index</h1>

@if (count($posts) > 0)
<ul>
    @foreach ($posts as $post)
    <a href="{{ route('post.show', $post['id']) }}">
        <li>{{ $post['title'] }}</li>
    </a>
    
    @endforeach
</ul>
@else
<p>Tidak ada post</p>
@endif

@endsection


