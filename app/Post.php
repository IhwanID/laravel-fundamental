<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Post extends Model
{

    //menggunakan softdelete
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    //protected $table = 'post';
    //protected $primaryKey = 'post_id';

    //mass assignment
    protected $fillable = ['title', 'body', 'user_id'];

    
}
