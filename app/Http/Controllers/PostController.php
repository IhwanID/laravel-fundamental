<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Template Resource
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = [
            ['id' => '1', 'title' => 'Post pertama', 'body' => 'Isi dari post pertama'],
            ['id' => '2', 'title' => 'Post kedua', 'body' => 'Isi dari post kedua'],
            ['id' => '3', 'title' => 'Post ketiga', 'body' => 'Isi dari post ketiga'],
            ['id' => '4', 'title' => 'Post keempat', 'body' => 'Isi dari post keempat'],
            ['id' => '5', 'title' => 'Post kelima', 'body' => 'Isi dari post kelima'],
        ];

        return view('index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 'halaman show dari id '. $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return 'halaman edit dari id '. $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
