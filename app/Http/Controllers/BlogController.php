<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Tanpa template resource
class BlogController extends Controller
{
    public function index(){
        $posts = [
            ['id' => '1', 'title' => 'Post pertama', 'body' => 'Isi dari post pertama'],
            ['id' => '2', 'title' => 'Post kedua', 'body' => 'Isi dari post kedua'],
            ['id' => '3', 'title' => 'Post ketiga', 'body' => 'Isi dari post ketiga'],
            ['id' => '4', 'title' => 'Post keempat', 'body' => 'Isi dari post keempat'],
            ['id' => '5', 'title' => 'Post kelima', 'body' => 'Isi dari post kelima'],
        ];
    
        echo '<ul>';
        foreach($posts as $post){
            echo '<li> <a href="'. route('blog.detail', $post['id']) . '">' . $post['title']. '</li>';
        }
        echo '</ul>';
    }

    public function create(){
        return view('create');
    }

    public function store(Request $request){
        return dd($request->all());
    }


}

