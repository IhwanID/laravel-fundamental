<?php

use App\Http\Controllers\BlogController;
use Illuminate\Support\Facades\DB;
use App\Post;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* 
Semua route terletak disini
contoh menambahkan route baru:

Route::get('/hello ', function () {
    return 'hallo semuanya';
});

*/


Route::get('/', function () {
    return view('welcome');
});

//route tanpa controller (closure function)
Route::get('/blog-old', function () {
    $posts = [
        ['id' => '1', 'title' => 'Post pertama', 'body' => 'Isi dari post pertama'],
        ['id' => '2', 'title' => 'Post kedua', 'body' => 'Isi dari post kedua'],
        ['id' => '3', 'title' => 'Post ketiga', 'body' => 'Isi dari post ketiga'],
        ['id' => '4', 'title' => 'Post keempat', 'body' => 'Isi dari post keempat'],
        ['id' => '5', 'title' => 'Post kelima', 'body' => 'Isi dari post kelima'],
    ];
    
    echo '<ul>';
    foreach($posts as $post){
        echo '<li> <a href="'. route('blog.detail', $post['id']) . '">' . $post['title']. '</li>';
    }
    echo '</ul>';
});

Route::get('/blog-old/{id}', function($id){
    echo 'post '. $id;
})->name('blog.detail');

// Route dengan controller
Route::get('/blog', 'BlogController@index');
Route::get('/blog/create', 'BlogController@create');
Route::post('/blog', 'BlogController@store');

Route::resource('post', 'PostController');

//RAW SQL Query
Route::get('/insert', function(){
    DB::insert('insert into posts(title, body, user_id) values(?,?,?)', ['Belajar PHP', 'Mudah banget', '2' ]);
    echo('data berhasil ditambah');
});

Route::get('/tambah', function(){
    //cara lain untuk insert (chain method)
    $data = [
        'title' => 'Judul nya ini',
        'body' => 'isiannya ini',
        'user_id' => 4
    ];
    DB::table('posts')->insert($data);
    echo('data berhasil ditambah');
});

Route::get('/read', function () {
    //raw query without chain method 
    //$query = DB::select('select * from posts where id = ?', [1]);
    
    //with chain methods
    //first() = object
    $query = DB::table('posts')->select('title', 'body')->where('id', 1)->get();
    return dd($query);
    //return var_dump($query);
});

Route::get('/update', function () {
    //$update = DB::update('update posts set title = "judul terbaru" where id = ?', [2]);

    $data = [
        'title' => 'Judul terbaru saat ini',
        'body' => 'isiannya terbaru',
        'user_id' => 4
    ];

    $update = DB::table('posts')->where('id', 1)->update($data);
    return $update;
});

Route::get('/delete', function () {
    //$delete = DB::delete('delete from posts where id = ?', [2]);

    $delete = DB::table('posts')->where('id', 1)->delete();

    return $delete;
});

//Eloquent
Route::get('/posts', function () {
    $posts = Post::all();
    return $posts;
});

Route::get('/find', function () {
    $posts = Post::find(5);
    return $posts;
});

Route::get('/findWhere', function () {
    //->take(1) = berapa data yang dibutuhkan
    $posts = Post::where('user_id', 2)->orderBy('id', 'desc')->get();
    return $posts;
});

Route::get('/create', function () {
    //insert data dengan eloquent otomatis mengisi timestamp
    $post = new Post();
    $post->title = 'Judulnya Dilan 1990';
    $post->body = 'Isinya bagus banget';
    // menambahkan id dari user login
    $post->user_id = Auth::user()->id;
    $post->save();

    return $post;
});

Route::get('/createpost', function () {
    $post = Post::create([
        'title' => 'Judul terbaru saat ini',
        'body' => 'isiannya terbaru banget',
        'user_id' => 4
    ]);
});

Route::get('/updatepost', function () {
    // $post = Post::find(4);
    $post = Post::where('id', 6);
    $post->update([
        'title' => 'Judul Bukan 66 terbaru saat ini',
        'body' => 'isiannya bukan terbaru banget',
        'user_id' => 4
    ]);
});

Route::get('/deletepost', function () {
    // $post = Post::find(4);
    // $post = Post::where('id', 6);
    // $post->delete();

    // Post::destroy(5);
    Post::destroy([3,4,7]);
});

Route::get('/softdelete', function () {
    Post::destroy(9);
});
// data yang berada di trash
Route::get('/trash', function () {
    // query menampilkan dengan trash 
    // Post::withTrashed()->get();

    return Post::onlyTrashed()->get();
});

Route::get('/restore', function () {
    // query merestore data 
    $query = Post::onlyTrashed()->restore();
    
    return $query;
});

// force deletes
Route::get('/forceDelete', function () {
    // jika ingin langsung menghapus
    // Post::find(1)->forceDelete();
    
    $query = Post::onlyTrashed()->where('id', 9)->forceDelete();
    return 'berhasil';
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/user', 'HomeController@user');

Route::get('/admin', function () {
    return 'halaman admin';
})->middleware('role', 'auth');

Route::get('/member', function () {
    return 'halaman member';
});